@extends('layouts.master')
@section('sidebar')
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                    
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">
                                    <img id="Img" src="{{asset('img/Personal/Ft_Personal.jpg')}}"><br>
                                    {{Auth::User()->name}}
                            </span>                            
                        </span>
                    </a>

                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{ URL::to('logout') }}">Salir</a></li>
                        <li class="divider"></li>
                    </ul>
                </div>
                <div class="logo-element">
                    CCM+
                </div>
            </li>
            <li>
                <a href="{{ URL::to('admin') }}">
                    <i class="fa fa-dashboard"></i>
                    <span class="nav-label">Contenido</span>
                </a>
            </li>

            <li class="">
                <a href="{{ URL::to('admin/job-list') }}">
                    <i class="fa fa-sliders"></i>
                    <span class="nav-label">Trabajos</span> <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ URL::to('admin/job-list') }}" >
                            Lista
                            <span class="label label-primary pull-right">
                                <i class="fa fa-bars"></i>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::to('admin/job/post-new-job') }}">
                            Nuevo Trabajo
                            <span class="label label-primary pull-right">Nuevo</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::to('admin/job/search-job') }}" >
                            Buscar Trabajo
                            <span class="label label-primary pull-right">
                                <i class="fa fa-search"></i>
                            </span>
                        </a>
                    </li>

                </ul>
            </li>

            <li class="">
                <a href="{{ URL::to('admin/department-list') }}">
                    <i class="fa fa-sliders"></i>
                    <span class="nav-label">Departamentos</span> <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ URL::to('admin/department-list') }}" >
                            Lista
                            <span class="label label-primary pull-right">
                                <i class="fa fa-bars"></i>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::to('admin/department/add-new-department') }}">
                            Nuevo Departamento
                            <span class="label label-primary pull-right">Nuevo</span>
                        </a>
                    </li>
                </ul>
            </li>


            <li class="">
                <a href="{{ URL::to('admin/application-list') }}">
                    <i class="fa fa-sliders"></i>
                    <span class="nav-label">Aplicaciones</span> <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ URL::to('admin/application-list') }}" >
                            Lista
                            <span class="label label-primary pull-right">
                                <i class="fa fa-bars"></i>
                            </span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ URL::to('admin/application-search') }}" >
                            Buscar Application
                            <span class="label label-primary pull-right">
                                <i class="fa fa-search"></i>
                            </span>
                        </a>
                        
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
@endsection