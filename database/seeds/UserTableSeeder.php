<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'name' => ' Jhon',
            'email' => 'jhon.gonzal.98@gmail.com',
            'role' => 'ADMIN',
            'password' => bcrypt('1024594341E'),
        ]);
    }
}
